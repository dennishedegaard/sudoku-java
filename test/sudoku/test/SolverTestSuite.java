package sudoku.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import junit.framework.JUnit4TestAdapter;
import junit.framework.Test;

@SuppressWarnings("deprecation")
@RunWith(Suite.class)
@SuiteClasses({ SolverTest_check_board.class,
		SolverTest_check_board_delta.class, SolverTest_solve.class })
/**
 * A {@link TestSuite} with all the tests in the project.
 * 
 * @author Dennis Hedegaard
 * 
 */
public class SolverTestSuite {
	public static Test suite() {
		return new JUnit4TestAdapter(SolverTestSuite.class);
	}
}
