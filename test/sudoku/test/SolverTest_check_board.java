package sudoku.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import sudoku.lib.Solver;

/**
 * junit test cases for the {@link Solver#check_board(int[][])} method.
 * <p>
 * Please note that the algorithm is deprecated for performance reasons.
 * </p>
 * 
 * @author Dennis Hedegaard
 * @see Solver#check_board(int[][])
 */
@Deprecated
public class SolverTest_check_board {
	/**
	 * Checks the sudoku found at <a href=
	 * "http://www.ibm.com/developerworks/xml/library/x-xformssudoku1/index.html"
	 * >http://www.ibm.com/developerworks/xml/library/x-xformssudoku1/index.html
	 * </a>
	 * <p>
	 * Figure 2, a sudoku gme in a solved and valid state, except for the error
	 * expressed below.
	 * </p>
	 * <p>
	 * However there's a wrong number in the grid at 6,6 where that 1 should be
	 * a 2.
	 * </p>
	 */
	@Test
	public void testCheck_board_valid1() {
		assertEquals(
				Solver.BOARD_STATE.SOLVED_VALID,
				Solver.check_board(new int[][] { { 3, 2, 9, 6, 5, 7, 8, 4, 1 },
						{ 7, 4, 5, 8, 3, 1, 2, 9, 6 },
						{ 6, 1, 8, 2, 4, 9, 3, 7, 5 },
						{ 1, 9, 3, 4, 6, 8, 5, 2, 7 },
						{ 2, 7, 6, 1, 9, 5, 4, 8, 3 },
						{ 8, 5, 4, 3, 7, 2, 6, 1, 9 },
						{ 4, 3, 2, 7, 1, 6, 9, 5, 8 },
						{ 5, 8, 7, 9, 2, 3, 1, 6, 4 },
						{ 9, 6, 1, 5, 8, 4, 7, 3, 2 } }));
	}

	/**
	 * Checks the sudoku found at <a
	 * href="http://www.kosbie.net/cmu/fall-08/15-100/handouts/hw7.html"
	 * >http://www.kosbie.net/cmu/fall-08/15-100/handouts/hw7.html</a>
	 * <p>
	 * under "b. isSudokuRectangle", the sudoku game in the solved and valid
	 * state.
	 * </p>
	 */
	@Test
	public void testCheck_board_valid2() {
		assertEquals(
				Solver.BOARD_STATE.SOLVED_VALID,
				Solver.check_board(new int[][] { { 5, 3, 4, 6, 7, 8, 9, 1, 2 },
						{ 6, 7, 2, 1, 9, 5, 3, 4, 8 },
						{ 1, 9, 8, 3, 4, 2, 5, 6, 7 },
						{ 8, 5, 9, 7, 6, 1, 4, 2, 3 },
						{ 4, 2, 6, 8, 5, 3, 7, 9, 1 },
						{ 7, 1, 3, 9, 2, 4, 8, 5, 6 },
						{ 9, 6, 1, 5, 3, 7, 2, 8, 4 },
						{ 2, 8, 7, 4, 1, 9, 6, 3, 5 },
						{ 3, 4, 5, 2, 8, 6, 1, 7, 9 } }));
	}

	/**
	 * Checks the sudoku found at <a
	 * href="http://projecteuler.net/index.php?section=problems&id=96"
	 * >http://projecteuler.net/index.php?section=problems&id=96</a>
	 */
	@Test
	public void testCheck_board_valid3() {
		assertEquals(
				Solver.BOARD_STATE.SOLVED_VALID,
				Solver.check_board(new int[][] { { 4, 8, 3, 9, 2, 1, 6, 5, 7 },
						{ 9, 6, 7, 3, 4, 5, 8, 2, 1 },
						{ 2, 5, 1, 8, 7, 6, 4, 9, 3 },
						{ 5, 4, 8, 1, 3, 2, 9, 7, 6 },
						{ 7, 2, 9, 5, 6, 4, 1, 3, 8 },
						{ 1, 3, 6, 7, 9, 8, 2, 4, 5 },
						{ 3, 7, 2, 6, 8, 9, 5, 1, 4 },
						{ 8, 1, 4, 2, 5, 3, 7, 6, 9 },
						{ 6, 9, 5, 4, 1, 7, 3, 8, 2 } }));
	}

	/**
	 * Checks the sudoku found at <a href=
	 * "http://www.ibm.com/developerworks/xml/library/x-xformssudoku1/index.html"
	 * >http://www.ibm.com/developerworks/xml/library/x-xformssudoku1/index.html
	 * </a>
	 * <p>
	 * Figure 1, a sudoku game in an unsolved but valid state.
	 * </p>
	 */
	@Test
	public void testCheck_board_valid_unsolved1() {
		assertEquals(
				Solver.BOARD_STATE.UNSOLVED_VALID,
				Solver.check_board(new int[][] { { 0, 2, 9, 0, 0, 0, 0, 4, 0 },
						{ 7, 0, 5, 8, 0, 0, 0, 0, 6 },
						{ 0, 0, 0, 2, 0, 0, 3, 7, 5 },
						{ 0, 9, 3, 0, 6, 8, 5, 0, 0 },
						{ 0, 7, 0, 0, 9, 0, 0, 8, 0 },
						{ 0, 0, 4, 3, 7, 0, 6, 1, 0 },
						{ 4, 3, 2, 0, 0, 6, 0, 0, 0 },
						{ 5, 0, 0, 0, 0, 3, 1, 0, 4 },
						{ 0, 6, 0, 0, 0, 0, 7, 3, 0 } }));
	}

	/**
	 * Checks the sudoku found at <a
	 * href="http://www.kosbie.net/cmu/fall-08/15-100/handouts/hw7.html"
	 * >http://www.kosbie.net/cmu/fall-08/15-100/handouts/hw7.html</a>
	 * <p>
	 * under "b. isSudokuRectangle" the sudoku in the unsolved but valid state.
	 * </p>
	 */
	@Test
	public void testCheck_board_valid_unsolved2() {
		assertEquals(
				Solver.BOARD_STATE.UNSOLVED_VALID,
				Solver.check_board(new int[][] { { 5, 3, 0, 0, 7, 0, 0, 0, 0 },
						{ 6, 0, 0, 1, 9, 5, 0, 0, 0 },
						{ 0, 9, 8, 0, 0, 0, 0, 6, 0 },
						{ 8, 0, 0, 0, 6, 0, 0, 0, 3 },
						{ 4, 0, 0, 8, 0, 3, 0, 0, 1 },
						{ 7, 0, 0, 0, 2, 0, 0, 0, 6 },
						{ 0, 6, 0, 0, 0, 0, 2, 8, 0 },
						{ 0, 0, 0, 4, 1, 9, 0, 0, 5 },
						{ 0, 0, 0, 0, 8, 0, 0, 7, 9 } }));
	}

	/**
	 * Checks the sudoku found at <a
	 * href="http://projecteuler.net/index.php?section=problems&id=96"
	 * >http://projecteuler.net/index.php?section=problems&id=96</a>
	 * <p>
	 * This is the unsolved, valid sudoku.
	 * </p>
	 */
	@Test
	public void testCheck_board_valid_unsolved3() {
		assertEquals(
				Solver.BOARD_STATE.UNSOLVED_VALID,
				Solver.check_board(new int[][] { { 0, 0, 3, 0, 2, 0, 6, 0, 0 },
						{ 9, 0, 0, 3, 0, 5, 0, 0, 1 },
						{ 0, 0, 1, 8, 0, 6, 4, 0, 0 },
						{ 0, 0, 8, 1, 0, 2, 9, 0, 0 },
						{ 7, 0, 0, 0, 0, 0, 0, 0, 8 },
						{ 0, 0, 6, 7, 0, 8, 2, 0, 0 },
						{ 0, 0, 2, 6, 0, 9, 5, 0, 0 },
						{ 8, 0, 0, 2, 0, 3, 0, 0, 9 },
						{ 0, 0, 5, 0, 1, 0, 3, 0, 0 } }));
	}

	/**
	 * Checks the sudoku found at <a href=
	 * "http://www.ibm.com/developerworks/xml/library/x-xformssudoku1/index.html"
	 * >http://www.ibm.com/developerworks/xml/library/x-xformssudoku1/index.html
	 * </a>
	 * <p>
	 * Figure 2, a sudoku gme in a solved and valid state, except for a random
	 * error at the beginning. 1,1: 3->1
	 * </p>
	 * <p>
	 * Same as with {@link #testCheck_board_valid1()}: However there's a wrong
	 * number in the grid at 6,6 where that 1 should be a 2.
	 * </p>
	 */
	@Test
	public void testCheck_board_invalid1() {
		assertEquals(
				Solver.BOARD_STATE.INVALID,
				Solver.check_board(new int[][] { { 1, 2, 9, 6, 5, 7, 8, 4, 1 },
						{ 7, 4, 5, 8, 3, 1, 2, 9, 6 },
						{ 6, 1, 8, 2, 4, 9, 3, 7, 5 },
						{ 1, 9, 3, 4, 6, 8, 5, 2, 7 },
						{ 2, 7, 6, 1, 9, 5, 4, 8, 3 },
						{ 8, 5, 4, 3, 7, 2, 6, 1, 9 },
						{ 4, 3, 2, 7, 1, 6, 9, 5, 8 },
						{ 5, 8, 7, 9, 2, 3, 1, 6, 4 },
						{ 9, 6, 1, 5, 8, 4, 7, 3, 2 } }));
	}

	/**
	 * Checks the sudoku found at <a href=
	 * "http://www.ibm.com/developerworks/xml/library/x-xformssudoku1/index.html"
	 * >http://www.ibm.com/developerworks/xml/library/x-xformssudoku1/index.html
	 * </a>
	 * <p>
	 * Figure 1, a sudoku game in an unsolved and invalid state.
	 * </p>
	 * <p>
	 * changed 4,2: 8->7
	 * </p>
	 */
	@Test
	public void testCheck_board_invalid2() {
		assertEquals(
				Solver.BOARD_STATE.INVALID,
				Solver.check_board(new int[][] { { 0, 2, 9, 0, 0, 0, 0, 4, 0 },
						{ 7, 0, 5, 7, 0, 0, 0, 0, 6 },
						{ 0, 0, 0, 2, 0, 0, 3, 7, 5 },
						{ 0, 9, 3, 0, 6, 8, 5, 0, 0 },
						{ 0, 7, 0, 0, 9, 0, 0, 8, 0 },
						{ 0, 0, 4, 3, 7, 0, 6, 1, 0 },
						{ 4, 3, 2, 0, 0, 6, 0, 0, 0 },
						{ 5, 0, 0, 0, 0, 3, 1, 0, 4 },
						{ 0, 6, 0, 0, 0, 0, 7, 3, 0 } }));
	}
}
