package sudoku.test;

import static org.junit.Assert.*;

import org.junit.Test;

import sudoku.lib.Solver;

/**
 * JUnit 4.x tests for the {@link Solver#check_board_delta(int...)} method.
 * 
 * @author Dennis Hedegaard
 * @see Solver#check_board_delta(int...)
 */
public class SolverTest_check_board_delta {
	/**
	 * A simple valid check for a full board sorted.
	 */
	@Test
	public void testCheck_board_delta_valid1() {
		assertEquals(Solver.BOARD_STATE.SOLVED_VALID,
				Solver.check_board_delta(1, 2, 3, 4, 5, 6, 7, 8, 9));
	}

	/**
	 * A simple valid check for a full board reverse-sorted.
	 */
	@Test
	public void testCheck_board_delta_valid2() {
		assertEquals(Solver.BOARD_STATE.SOLVED_VALID,
				Solver.check_board_delta(9, 8, 7, 6, 5, 4, 3, 2, 1));
	}

	/**
	 * A simple valid check for a full board in random order.
	 */
	@Test
	public void testCheck_board_delta_valid3() {
		assertEquals(Solver.BOARD_STATE.SOLVED_VALID,
				Solver.check_board_delta(5, 3, 7, 8, 9, 1, 4, 2, 6));
	}

	/**
	 * A simple valid check for a full board in random order.
	 */
	@Test
	public void testCheck_board_delta_valid4() {
		assertEquals(Solver.BOARD_STATE.SOLVED_VALID,
				Solver.check_board_delta(2, 7, 6, 1, 9, 5, 4, 8, 3));
	}

	/**
	 * A simple valid check for a full board sorted, first spot 0.
	 */
	@Test
	public void testCheck_board_delta_valid_unsolved1() {
		assertEquals(Solver.BOARD_STATE.UNSOLVED_VALID,
				Solver.check_board_delta(0, 2, 3, 4, 5, 6, 7, 8, 9));
	}

	/**
	 * A simple valid check for a full board reverse-sorted, first spot 0.
	 */
	@Test
	public void testCheck_board_delta_valid_unsolved2() {
		assertEquals(Solver.BOARD_STATE.UNSOLVED_VALID,
				Solver.check_board_delta(0, 8, 7, 6, 5, 4, 3, 2, 1));
	}

	/**
	 * A simple valid check for a full board in random order, first spot 0.
	 */
	@Test
	public void testCheck_board_delta_valid_unsolved3() {
		assertEquals(Solver.BOARD_STATE.UNSOLVED_VALID,
				Solver.check_board_delta(0, 3, 7, 8, 9, 1, 4, 2, 6));
	}

	/**
	 * A simple valid check for a full board sorted, first spot 0.
	 */
	@Test
	public void testCheck_board_delta_valid_unsolved4() {
		assertEquals(Solver.BOARD_STATE.UNSOLVED_VALID,
				Solver.check_board_delta(1, 2, 3, 4, 5, 6, 7, 8, 0));
	}

	/**
	 * A simple valid check for a full board reverse-sorted, first spot 0.
	 */
	@Test
	public void testCheck_board_delta_valid_unsolved5() {
		assertEquals(Solver.BOARD_STATE.UNSOLVED_VALID,
				Solver.check_board_delta(9, 8, 7, 6, 5, 4, 3, 2, 0));
	}

	/**
	 * A simple valid check for a full board in random order, first spot 0.
	 */
	@Test
	public void testCheck_board_delta_valid_unsolved6() {
		assertEquals(Solver.BOARD_STATE.UNSOLVED_VALID,
				Solver.check_board_delta(5, 3, 7, 8, 9, 1, 4, 2, 0));
	}

	/**
	 * A simple valid check for a full board sorted, middle spot 0.
	 */
	@Test
	public void testCheck_board_delta_valid_unsolved7() {
		assertEquals(Solver.BOARD_STATE.UNSOLVED_VALID,
				Solver.check_board_delta(1, 2, 3, 4, 0, 6, 7, 8, 9));
	}

	/**
	 * A simple valid check for a full board reverse-sorted, middle spot 0.
	 */
	@Test
	public void testCheck_board_delta_valid_unsolved8() {
		assertEquals(Solver.BOARD_STATE.UNSOLVED_VALID,
				Solver.check_board_delta(9, 8, 7, 6, 0, 4, 3, 2, 1));
	}

	/**
	 * A simple valid check for a full board in random order, middle spot 0.
	 */
	@Test
	public void testCheck_board_delta_valid_unsolved9() {
		assertEquals(Solver.BOARD_STATE.UNSOLVED_VALID,
				Solver.check_board_delta(5, 3, 7, 8, 0, 1, 4, 2, 6));
	}

	/**
	 * A simple invalid check, 2 x 3's in the middle.
	 */
	@Test
	public void testCheck_board_delta_invalid1() {
		assertEquals(Solver.BOARD_STATE.INVALID,
				Solver.check_board_delta(5, 3, 7, 8, 3, 1, 4, 2, 6));
	}

	/**
	 * A simple invalid check, 2 x 3's in the beginning.
	 */
	@Test
	public void testCheck_board_delta_invalid2() {
		assertEquals(Solver.BOARD_STATE.INVALID,
				Solver.check_board_delta(5, 5, 7, 8, 3, 1, 4, 2, 6));
	}

	/**
	 * A simple invalid check, 2 x 3's in the beginning.
	 */
	@Test
	public void testCheck_board_delta_invalid3() {
		assertEquals(Solver.BOARD_STATE.INVALID,
				Solver.check_board_delta(5, 6, 7, 8, 3, 1, 4, 2, 5));
	}
}
